import pandas
import numpy as np
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

# Chargement du csv
data = pandas.read_csv("HeartDiseaseUCI.csv")

# Préparation de la colonne thal
X = data[['thal']]
X = np.nan_to_num(X)

# OneHotEncoding de la colonne thal
enc = OneHotEncoder()
X = enc.fit_transform(X).toarray()

# Ajouter des noms de colonnes à nos variables transformées
X_df = pandas.DataFrame(X)
X_df.columns = [
    'no_thal',
    'normal_thal',
    'fd_thal',
    'rd_thal'
]

# Assembler les données
data = data.join(X_df)


#
# KNN
#
# On va faire l'opération 100 fois pour voir quel meilleur nombre de voisins revient souvent
def knn(X, y):
    n_neighbors = []

    for _ in range(100):
        error = []

        # Séparer les données d'entrainement et de test
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)

        # Feature scaling : pas compris ce qu'il se passe mais il parait que c'est bien
        scaler = StandardScaler()
        scaler.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)

        # On teste sur le nombre de voisins entre 1 et 40
        for i in range(1, 40):
            # On entraîne notre jeu de données
            knn = KNeighborsClassifier(n_neighbors=i)
            knn.fit(X_train, y_train.values.ravel())
            # La valeur que l'algorithme suppose être la bonne
            pred_i = knn.predict(X_test)

            # Score <- la probabilité que le bon résultat ressorte
            # score = knn.score(X_test, y_test)

            error.append(
                np.mean([pred_i] != y_test)
            )

        # On prend le meilleur voisin qui a le taux d'erreur le plus bas
        n_neighbors.append(np.argmin(error) + 1)

    # 18 revient très souvent
    print(np.average(n_neighbors))


def svm(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)

    clf = SVC(kernel='linear')
    clf.fit(X_train, y_train.values.ravel())

    y_pred = clf.predict(X_test)
    print(y_pred)

    print("Accuracy:", metrics.accuracy_score(y_test, y_pred))


X = data.drop(columns=['index', 'num', 'thal', 'sex'])
y = data[['num']]

# knn(X, y)
svm(X, y)